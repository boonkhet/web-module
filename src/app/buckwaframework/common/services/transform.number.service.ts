import { Injectable } from '@angular/core';
@Injectable()
export class TransformIntegerService  {
    constructor() {

    }
    addComma(data: any) {
        if(data != undefined)
            data =  data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        return data;
    }

    removeComma(data: any) {
        if(data != undefined)
            data = data.toString().replace(/,/g, "");

        return data;
    }

}
