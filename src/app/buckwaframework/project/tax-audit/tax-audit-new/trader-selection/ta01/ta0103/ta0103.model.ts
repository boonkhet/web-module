export interface TaxOperatorFormVo {
  analysisNumber: string;
  dateStart: string;
  dateEnd: string;
  dateRange: number;
  draftNumber: string;
  budgetYear: string;
  start: number;
  length: number;
  facType: string;
  dutyCode: string;
  officeCode: string;
  condNumber: string;
  taxType: string;
  sumTaxAmStart: number;
  sumTaxAmEnd: number;
  newRegId:string;
}
