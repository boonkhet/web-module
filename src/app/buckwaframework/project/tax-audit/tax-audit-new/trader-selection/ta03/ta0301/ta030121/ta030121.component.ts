import { Component, OnInit } from '@angular/core';
import { TextDateTH, formatter, converDate, patternDate } from 'helpers/datepicker';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AjaxService } from 'services/ajax.service';
import { Store } from '@ngrx/store';
import { Ta0301, ListFormTsNumber } from '../ta0301.model';
import { MessageService } from 'services/message.service';
import { ResponseData } from 'models/response-data.model';
import { MessageBarService } from 'services/message-bar.service';
import * as moment from 'moment';
import * as TA0301ACTION from "../ta0301.action";
import { Ta0301Service } from '../ts0301.service';
import { Utils } from 'helpers/utils';
import { PathTs } from '../path.model';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

declare var $: any;

const URL = {
  EXPORT: AjaxService.CONTEXT_PATH + "ta/report/form-ts/pdf/ta-form-ts0121/",
  DATA_TABLE_LIST: 'person-info/person-info-list',
  SEARCH_DEPARTMENT: AjaxService.CONTEXT_PATH + "preferences/department/sector-list-flag"
}
@Component({
  selector: 'app-ta030121',
  templateUrl: './ta030121.component.html',
  styleUrls: ['./ta030121.component.css']
})
export class Ta030121Component implements OnInit {
  submit: boolean = false;
  loading: boolean = false;
  pathTs: any;
  listFormTsNumber: ListFormTsNumber;
  dataStore: any;
  taFormTS0121: FormGroup;
  formTsNumber: any;

  auditPlanCode: string = "";
  auditStepStatus: string = "";

  ta0301: Ta0301
  //data table
  dataList: any[] = [];
  table: any;
  searchModalNumber: any;

  flagDepartment: string = "";
  tableDep: any;

  constructor(
    private fb: FormBuilder,
    private messageBar: MessageBarService,
    private store: Store<AppState>,
    private ajax: AjaxService,
    private ta0301Service: Ta0301Service,
    private route: ActivatedRoute
  ) {
    this.setTaFormTS0121();
    this.getDataListForSearchModal();
  }

  // ================== Initial settting =====================
  ngOnInit() {
    this.auditPlanCode = this.route.snapshot.queryParams['auditPlanCode'] || "";
    this.auditStepStatus = this.route.snapshot.queryParams['auditStepStatus'] || "";
    this.taFormTS0121.patchValue({
      auditPlanCode: this.auditPlanCode,
      auditStepStatus: this.auditStepStatus
    })
    this.dataStore = this.store.select(state => state.Ta0301.ta0301).subscribe(datas => {
      this.taFormTS0121.get('formTsNumber').patchValue(datas.formTsNumber);
      this.pathTs = datas.pathTs;
      const pathModel = new PathTs();
      this.ta0301Service.checkPathFormTs(this.pathTs, pathModel.ts0121)
      console.log("store =>", datas)
      if (Utils.isNotNull(this.taFormTS0121.get('formTsNumber').value)) {
        this.getFormTs(this.taFormTS0121.get('formTsNumber').value);
      } else {
        this.clear('');
        setTimeout(() => {
          this.taFormTS0121.patchValue({
            auditPlanCode: this.auditPlanCode,
            auditStepStatus: this.auditStepStatus
          })
        }, 200);
      }
    });
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.   
    this.callCalendarDefault('docDate', 'docDate', 'date');
    this.callCalendarDefault('comdDate', 'comdDate', 'date');
  }
  callCalendarDefault(id: string, formControlName: string, type: string): void {
    $(`#${id}`).calendar({
      maxDate: new Date(),
      type: `${type}`,
      text: TextDateTH,
      formatter: formatter('วดป'),
      onChange: (date, text) => {
        this.taFormTS0121.get(`${formControlName}`).patchValue(text);
      }
    });
  }

  setTaFormTS0121() {
    this.taFormTS0121 = this.fb.group({
      formTsNumber: [""],
      factoryName: [""],
      officerSendFullName1: [""],
      officerSendPosition1: [""],
      officerReceiveFullName1: [""],
      officerReceivePosition1: [""],
      officeName: [""],
      docDate: [""],
      comdDesc: [""],
      comdDate: [""],
      officerSendFullName2: [""],
      factoryName2: [""],
      officerReceiveFullName2: [""],
      officerSendFullName3: [""],
      officerReceiveFullName3: [""],
      factoryName3: [""],
      doc1Num: [""],
      docAcct1Num: [""],
      docAcct1No: [""],
      docAcct2Num: [""],
      docAcct2No: [""],
      docOther: [""],
      signOfficerFullName1: [""],
      signOfficerFullName2: [""],
      signWitnessFullName1: [""],
      signWitnessFullName2: [""],
      auditPlanCode: [""],
      auditStepStatus: [""]
    })
  }

  getFormTs(formTsNumber: string) {
    this.ajax.doPost(`ta/report/get-form-ts/ta-form-ts0121/${formTsNumber}`, {}).subscribe((res: ResponseData<any>) => {
      if (MessageService.MSG.SUCCESS == res.status) {
        console.log("getFormTs : ", res.data);
        let json = JSON.parse(res.data)
        this.taFormTS0121.patchValue({
          formTsNumber: json.formTsNumber,
          factoryName: json.factoryName,
          officerSendFullName1: json.officerSendFullName1,
          officerSendPosition1: json.officerSendPosition1,
          officerReceiveFullName1: json.officerReceiveFullName1,
          officerReceivePosition1: json.officerReceivePosition1,
          officeName: json.officeName,
          docDate: Utils.isNotNull(json.docDate) ? converDate(json.docDate, patternDate.DD_MM_YYYY) : '',
          comdDesc: json.comdDesc,
          comdDate: Utils.isNotNull(json.comdDate) ? converDate(json.comdDate, patternDate.DD_MM_YYYY) : '',
          officerSendFullName2: json.officerSendFullName2,
          factoryName2: json.factoryName2,
          officerReceiveFullName2: json.officerReceiveFullName2,
          officerSendFullName3: json.officerSendFullName3,
          officerReceiveFullName3: json.officerReceiveFullName3,
          factoryName3: json.factoryName3,
          doc1Num: json.doc1Num,
          docAcct1Num: json.docAcct1Num,
          docAcct1No: json.docAcct1No,
          docAcct2Num: json.docAcct2Num,
          docAcct2No: json.docAcct2No,
          docOther: json.docOther,
          signOfficerFullName1: json.signOfficerFullName1,
          signOfficerFullName2: json.signOfficerFullName2,
          signWitnessFullName1: json.signWitnessFullName1,
          signWitnessFullName2: json.signWitnessFullName2,

        })
      } else {
        this.messageBar.errorModal(res.message);
        console.log("Error !! getFormTsNumber ");
      }
    })
  }
  ngOnDestroy(): void {

    this.dataStore.unsubscribe();
  }
  // ===================== Action ===========================
  save(e) {
    this.messageBar.comfirm(res => {
      if (res) {
        this.loading = true;
        this.saveTs().subscribe((res: ResponseData<any>) => {
          this.messageBar.successModal(res.message);

          // ==> get list tsnumber
          this.getFromTsNumberList().subscribe(res => {
            this.getFormTs(this.taFormTS0121.get('formTsNumber').value)
            this.loading = false;
          });
        })
      }
    }, "ยืนยันการบันทึก");
  }

  export = e => {
    this.saveTs().subscribe((res: ResponseData<any>) => {
      this.getFromTsNumberList().subscribe(res => {

        this.getFormTs(this.taFormTS0121.get('formTsNumber').value);

        this.loading = false;
        this.submit = true;

        //export
        var form = document.createElement("form");
        form.method = "POST";
        // form.target = "_blank";
        form.action = URL.EXPORT;

        form.style.display = "none";
        var jsonInput = document.createElement("input");
        jsonInput.name = "json";
        jsonInput.value = JSON.stringify(this.taFormTS0121.value);
        form.appendChild(jsonInput);

        document.body.appendChild(form);
        form.submit();
      });
    });
  }

  clear(e) {
    this.taFormTS0121.reset();
  }

  getFromTsNumberList(): Observable<any> {
    return new Observable(obs => {
      let data = {
        auditPlanCode: this.auditPlanCode
      }
      this.ajax.doPost(`ta/report/form-ts-number/${this.pathTs}`, data).subscribe((res: ResponseData<any>) => {
        if (MessageService.MSG.SUCCESS == res.status) {
          this.listFormTsNumber = {
            listFormTsNumber: res.data
          }
          if (res.data.length != 0) {
            this.taFormTS0121.get('formTsNumber').patchValue(res.data[0]);
          }
          console.log(" getFromTsNumberList ==> : ", res.data)
          obs.next(res.data);
          this.store.dispatch(new TA0301ACTION.AddListFormTsNumber(this.listFormTsNumber))

          //==== set formTsNumber  to store =======
          setTimeout(() => {
            this.ta0301 = {
              formTsNumber: this.taFormTS0121.get('formTsNumber').value,
              pathTs: this.pathTs
            }
            this.store.dispatch(new TA0301ACTION.AddFormTsNumber(this.ta0301));
          }, 200);
          //!==== set formTsNumber  to store =======

        } else {
          this.messageBar.errorModal(res.message);
          console.log("Error !! getFormTsNumber 12");
        }
      })
    })
  }

  searchModalOpen(num: number) {
    $('#searchModal').modal('show');
    this.searchModalNumber = num;
    this.dataTable();
  }

  searchSubject() {
    $('#modalDepartment').modal('show');
    this.flagDepartment = '';
    this.tableDepartment();
  }

  // validateField(value: string) {
  //   return this.submit && this.taFormTS0121.get(value).errors;
  // }
  // ================ call back-end ====================
  saveTs(): Observable<any> {
    return new Observable(obs => {
      this.taFormTS0121.patchValue({
        auditPlanCode: this.auditPlanCode,
        auditStepStatus: this.auditStepStatus
      })
      // convert date DD MMMM YYYY to DD/MM/YYYY
      if (Utils.isNotNull(this.taFormTS0121.get('docDate').value)) {
        let docDate = converDate(this.taFormTS0121.get('docDate').value, patternDate.DD_MMMM_YYYY);
        this.taFormTS0121.get('docDate').patchValue(docDate);
      }
      if (Utils.isNotNull(this.taFormTS0121.get('comdDate').value)) {
        let comdDate = converDate(this.taFormTS0121.get('comdDate').value, patternDate.DD_MMMM_YYYY);
        this.taFormTS0121.get('comdDate').patchValue(comdDate);
      }
      this.ajax.doPost(`ta/report/save-form-ts/${this.pathTs}`, { json: JSON.stringify(this.taFormTS0121.value).toString() }).subscribe((res: ResponseData<any>) => {
        if (MessageService.MSG.SUCCESS == res.status) {
          obs.next(res);
        } else {
          this.messageBar.errorModal(res.message)
        }
      })
    })
  }

  getDataListForSearchModal() {
    this.ajax.doGet(URL.DATA_TABLE_LIST).subscribe((res: ResponseData<any>) => {
      if (MessageService.MSG.SUCCESS == res.status) {
        this.dataList = res.data;
      } else {
        this.messageBar.errorModal(res.message);
      }
    })
  }

  getDepartmentCheck(e) {
    this.flagDepartment = e;
    this.tableDep.ajax.reload();
  }

  // ================ data table ==============
  dataTable() {
    if (this.table != null) {
      this.table.destroy();
    }
    this.table = $("#tableModal").DataTableTh({
      lengthChange: true,
      searching: false,
      loading: true,
      ordering: false,
      pageLength: 10,
      processing: true,
      serverSide: false,
      paging: true,
      data: this.dataList,
      columns: [
        {
          render: function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
          },
          className: "text-center"
        }, {
          render: function (data, type, row, meta) {
            return row.personThName + " " + row.personThSurname;
          },
          className: "text-left"
        }, {
          data: "linePosition", className: "text-left"
        }
      ],
    });

    $.fn.DataTable.ext.pager.numbers_length = 5;

    this.table.on("dblclick", "tr", (event) => {
      let data = this.table.row($(event.currentTarget).closest("tr")).data();
      // console.log("double data => ", data);
      if (this.searchModalNumber == 1) {
        this.taFormTS0121.get('officerSendFullName1').patchValue(data.personThName + " " + data.personThSurname);
        this.taFormTS0121.get('officerSendPosition1').patchValue(data.linePosition);
      } else if (this.searchModalNumber == 2) {
        this.taFormTS0121.get('officerReceiveFullName1').patchValue(data.personThName + " " + data.personThSurname);
        this.taFormTS0121.get('officerReceivePosition1').patchValue(data.linePosition);
      }

      $('#searchModal').modal('hide');
    });

    this.table.on('draw.dt', function () {
      $('.paginate_button').not('.previous, .next').each(function (i, a) {
        var val = $(a).text();
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        $(a).text(val);
      })
    });
  }

  tableDepartment = () => {
    if (this.tableDep != null) {
      this.tableDep.destroy();
    }
    this.tableDep = $("#tbDepartment").DataTableTh({
      searching: false,
      loading: true,
      ordering: false,
      processing: true,
      serverSide: true,
      paging: true,
      scrollX: true,
      pageLength: 10,
      lengthChange: false,
      ajax: {
        type: "POST",
        url: URL.SEARCH_DEPARTMENT,
        contentType: "application/json",
        data: (d) => {
          return JSON.stringify($.extend({}, d, { 'flag': this.flagDepartment }));
        }
      },
      columns: [
        {
          render: (data, type, row, meta) => {
            return meta.row + meta.settings._iDisplayStart + 1;
          },
          className: "text-center"
        }, {
          data: "offCode",
          className: "text-center"
        }, {
          data: "offName",
          className: "text-left"
        }, {
          data: "offShortName",
          className: "text-left"
        }

      ]
    });


    this.tableDep.on("dblclick", "td", (event) => {
      var data = this.tableDep.row($(event.currentTarget).closest("tr")).data();
      this.taFormTS0121.get('officeName').patchValue(data.offName);
      $('#modalDepartment').modal('hide');
    });

    $.fn.DataTable.ext.pager.numbers_length = 5;

    this.table.on('draw.dt', function () {
      $('.paginate_button').not('.previous, .next').each(function (i, a) {
        var val = $(a).text();
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        $(a).text(val);
      })
    });

  }

}

class AppState {
  Ta0301: {
    ta0301: Ta0301
  }
}
