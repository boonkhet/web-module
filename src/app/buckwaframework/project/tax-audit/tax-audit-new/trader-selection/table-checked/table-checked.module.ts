import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableCheckedComponent } from './table-checked.component';
import { SharedModule } from 'app/buckwaframework/common/templates/shared.module';

@NgModule({
  declarations: [TableCheckedComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports:[TableCheckedComponent]
})
export class TableCheckedModule { }
