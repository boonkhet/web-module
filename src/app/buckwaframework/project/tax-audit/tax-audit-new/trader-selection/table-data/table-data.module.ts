import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableDataComponent } from './table-data.component';
import { SharedModule } from 'app/buckwaframework/common/templates/shared.module';
import { NgxPaginationModule } from 'ngx-pagination'; // pagination

@NgModule({
  declarations: [TableDataComponent],
  imports: [
    CommonModule,
    SharedModule,
    NgxPaginationModule // pagination
  ],
  exports:[TableDataComponent]
})
export class TableDataModule { }
