import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableTaxComponent } from './table-tax.component';
import { SharedModule } from 'app/buckwaframework/common/templates/shared.module';

@NgModule({
  declarations: [TableTaxComponent],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports:[TableTaxComponent]
})
export class TableTaxModule { }
